using System.Threading.Tasks;

namespace NawaDataTest
{

    public partial class Form1 : Form
    {
        private class Task
        {
            public List<int> TaskMingguan { get; set; }
            public int TotalTask { get; set; }
        }

        private int ParseNumber(object Number)
        {
            int number;
            if (int.TryParse(Number.ToString(), out number))
            {
                number = number;
            }
            else
            {
                number = 0;
            }
            return number;
        }

        private int HitungHasil(List<Task> data)
        {
            int bonusBintang = 0;
            int minimalTask = ParseNumber(txtPoint.Text);
            foreach (Task task in data)
            {
                bool cekTask = false;
                foreach (int taskHarian in task.TaskMingguan)
                {
                    if (taskHarian != minimalTask)
                    {
                        cekTask = false;
                        break;
                    }
                    cekTask = true;
                }
                if (cekTask)
                {
                    bonusBintang++;
                }

                if (task.TotalTask != 25)
                {
                    bonusBintang += -1;
                }
            }
            return bonusBintang;
        }

        public Form1()
        {
            InitializeComponent();
        }

        List<Task> tasks;
        


        private void txtPoint_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void txtTaskHarian_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && 
                !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.') &&
                (e.KeyChar != ' '))
                {
                    e.Handled = true;
                }
        }

        private void btnHitung_Click(object sender, EventArgs e)
        {
            List<int> taskHarian = new List<int>();
            int jumlahHariKerja = 5;
            List<List<int>> splitMingguan = new List<List<int>>();
            string inputTaskHarian = txtTaskHarian.Text;
            object[] objTaskHarian = inputTaskHarian.Trim().Select(c => (object)c).ToArray();

            foreach (object obj in objTaskHarian)
            {
                if (ParseNumber(obj) != 0)
                {
                    taskHarian.Add(ParseNumber(obj));
                }
            }

            for (int i = 0; i < taskHarian.Count; i += jumlahHariKerja)
            {
                if (i + jumlahHariKerja <= taskHarian.Count)
                {
                    List<int> splitTask = taskHarian.GetRange(i, jumlahHariKerja);
                    splitMingguan.Add(splitTask);
                }
                else
                {
                    List<int> splitTask = taskHarian.GetRange(i, taskHarian.Count - i);
                    splitMingguan.Add(splitTask);
                }
            }
            tasks = new List<Task>();
            foreach (List<int> list in splitMingguan)
            {
                Task data = new Task();
                data.TaskMingguan = list;
                data.TotalTask = list.Sum();
                tasks.Add(data);
            }
            lblHasil.Text = HitungHasil(tasks).ToString();


        }

        


    }
}