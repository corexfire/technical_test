﻿namespace NawaDataTest
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPoint = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTaskHarian = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblHasil = new System.Windows.Forms.Label();
            this.btnHitung = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input Minimal Point / hari :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPoint
            // 
            this.txtPoint.Location = new System.Drawing.Point(173, 11);
            this.txtPoint.Name = "txtPoint";
            this.txtPoint.Size = new System.Drawing.Size(40, 23);
            this.txtPoint.TabIndex = 1;
            this.txtPoint.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPoint_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Input task harian / minggu :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtTaskHarian
            // 
            this.txtTaskHarian.Location = new System.Drawing.Point(172, 40);
            this.txtTaskHarian.Multiline = true;
            this.txtTaskHarian.Name = "txtTaskHarian";
            this.txtTaskHarian.Size = new System.Drawing.Size(100, 83);
            this.txtTaskHarian.TabIndex = 2;
            this.txtTaskHarian.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTaskHarian_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Hasil :";
            // 
            // lblHasil
            // 
            this.lblHasil.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHasil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblHasil.Location = new System.Drawing.Point(172, 131);
            this.lblHasil.Name = "lblHasil";
            this.lblHasil.Size = new System.Drawing.Size(100, 23);
            this.lblHasil.TabIndex = 4;
            this.lblHasil.Text = ".....";
            // 
            // btnHitung
            // 
            this.btnHitung.Location = new System.Drawing.Point(278, 132);
            this.btnHitung.Name = "btnHitung";
            this.btnHitung.Size = new System.Drawing.Size(75, 23);
            this.btnHitung.TabIndex = 5;
            this.btnHitung.Text = "Hitung";
            this.btnHitung.UseVisualStyleBackColor = true;
            this.btnHitung.Click += new System.EventHandler(this.btnHitung_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 166);
            this.Controls.Add(this.btnHitung);
            this.Controls.Add(this.lblHasil);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTaskHarian);
            this.Controls.Add(this.txtPoint);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private TextBox txtPoint;
        private Label label2;
        private TextBox txtTaskHarian;
        private Label label3;
        private Label lblHasil;
        private Button btnHitung;
    }
}